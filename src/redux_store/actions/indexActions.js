import {
  SET_ITEMS_TO_ARRAY,
  REMOVE_ITEM,
  EDIT_ITEM,
  SEARCH_TEXT,
  SEARCH_UPDATE,
  SET_NAME,
  SET_MOBILE,
  SET_EMAIL,
  ADD_ITEMS_TO_ARRAY,
  DELETE_ITEMS_TO_ARRAY,
  EDIT_ITEMS_TO_ARRAY,
} from './types';
import store from '../store';

export const setItemsToArray = (name, id) => (dispatch) => {
  let state = store.getState().indexReducer;
  let data_row = state.data_row;
  data_row.push({
    name: name,
    id: id,
  });
  dispatch({
    type: SET_ITEMS_TO_ARRAY,
    payload: data_row,
  });
};

export const removeItems = (id) => (dispatch) => {
  let state = store.getState().indexReducer;
  let data_row = state.data_row;
  data_row = data_row.filter((item) => item.id !== id);
  dispatch({
    type: REMOVE_ITEM,
    payload: data_row,
  });
};

export const editItems = (name, id) => (dispatch) => {
  let state = store.getState().indexReducer;
  let data_row = [];
  state.data_row.map((item) => {
    if (item.id == id) {
      data_row.push({
        name: name,
        id: item.id,
      });
    } else {
      data_row.push({
        name: item.name,
        id: item.id,
      });
    }
  });
  dispatch({
    type: EDIT_ITEM,
    payload: data_row,
  });
};




export const searchUpdate = () => (dispatch) => {
  let state = store.getState().indexReducer;
  let txt = state.inputText
  let holder = state.data_row;
  let newData = holder.filter(function (item) {
    return item.name.includes(txt);
  });
  dispatch({
    type: SEARCH_UPDATE,
    payload: newData,
  });
};

export const setInput = (name) => (dispatch) => {
  dispatch({
    type: SEARCH_TEXT,
    payload: name,
  });
};
// export const hitAPI = search_text => dispatch => {
//   const search = search_text.trim();
//   if (search === '') return;
//   dispatch({type: SET_LOADING, payload: true});

//   let url = `https://api.flickr.com/services/rest/?method=flickr.photos.search&text=${search}&api_key=cb8e65679bb25463361e9da0faacad58&per_page=20&format=json&nojsoncallback=1&page=1`;

//   fetch(url, {
//     method: 'GET',
//   })
//     .then(response => response.json())
//     .then(responseJson => {
//       // //responseJson);
//       if (responseJson.stat == 'ok') {
//         // console.log(responseJson.thumb_url);
//         // // let markers = [];
//         let data = responseJson.photos.photo;
//         dispatch({type: ON_DATA_CHANGE, payload: data});
//         dispatch({type: SET_LOADING, payload: false});
//       } else {
//         alert('Zero result found');
//       }
//     })
//     .catch(error => {
//       console.error(error);
//     });
// };
export const setName = (name) => (dispatch) => {
  dispatch({
    type: SET_NAME,
    payload: name,
  });
};
export const setMobile = (mobile) => (dispatch) => {
  dispatch({
    type: SET_MOBILE,
    payload: mobile,
  });
};
export const setEmail = (email) => (dispatch) => {
  dispatch({
    type: SET_EMAIL,
    payload: email,
  });
};
export const addItemsToArray = (_name, _mobile, _email) => (dispatch) => {
  let state = store.getState().indexReducer;
  let data_row = state.data_row;
  data_row.push({
    name: _name,
    mobile: _mobile,
    email: _email
  })

  dispatch({
    type: ADD_ITEMS_TO_ARRAY,
    payload: data_row,
  });
};

export const deleteItemsToArray = (email) => (dispatch) => {
  let state = store.getState().indexReducer;
  let data = state.data_row;
  data = data.filter((item) => item.email !== email);
  console.log((data));
  dispatch({
    type: DELETE_ITEMS_TO_ARRAY,
    payload: data,
  });
};

export const EditItemsToArray = (name, email, mobile) => (dispatch) => {
  let state = store.getState().indexReducer;
  let data_row = state.data_row;
  let data=[]
   data_row.map((item) => {
    if (item.email == email) {
      data.push({
        name:name,
        mobile:mobile,
        email:item.email,
      })
    }else{
      data.push({
        name:item.name,
        mobile:item.mobile,
        email:item.email,
      })
    }
  })

  dispatch({
    type: EDIT_ITEMS_TO_ARRAY,
    payload: data,
  });
};
