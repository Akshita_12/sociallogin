import {

  ADD_ITEMS_TO_ARRAY,
  DELETE_ITEMS_TO_ARRAY,
  EDIT_ITEMS_TO_ARRAY,
  SEARCH_UPDATE, SET_EMAIL, SET_MOBILE, SET_NAME,
} from '../actions/types';

const initialState = {
  data_row: [
    {
      name: 'Jhon',
      mobile: '9887263721',
      email: 'jhon@gmail.com'
    },
    {
      name: 'Smith',
      mobile: '9887263721',
      email: 'smith@gmail.com'
    },
    {
      name: 'Rob',
      mobile: '9887263721',
      email: 'rob@gmail.com'
    },
    {
      name: 'Jessie',
      mobile: '9887263721',
      email: 'rob@gmail.com'
    },
    {
      name: 'Michael',
      mobile: '9887263721',
      email: 'michael@gmail.com'
    },
  ],
  dashboard_data: [
    {
      screen_name: 'Flatlist',
      icon: require('../../assets/react_icon.png')
    },
    {
      screen_name: 'Axios',
      icon: require('../../assets/react_icon.png')
    },
    {
      screen_name: 'Sqlite',
      icon: require('../../assets/react_icon.png')
    },
    {
      screen_name: 'Activity Indicator',
      icon: require('../../assets/react_icon.png')
    },
    {
      screen_name: 'Progress Bar',
      icon: require('../../assets/react_icon.png')
    },
    {
      screen_name: 'GoogleSignUp',
      icon: require('../../assets/react_icon.png')
    },
    {
      screen_name: 'GoogleSignUpCommunity',
      icon: require('../../assets/react_icon.png')
    },
    {
      screen_name: 'FacebookSignUpCommunity',
      icon: require('../../assets/react_icon.png')
    },
    {
      screen_name: 'InstagramSignUpCommunity',
      icon: require('../../assets/react_icon.png')
    },
    {
      screen_name: 'FacebookSignUp',
      icon: require('../../assets/react_icon.png')
    },
  ],
  name: '',
  mobile: '',
  email: ''
};

export default function (state = initialState, action) {

  switch (action.type) {

    case SEARCH_UPDATE:
      return {
        ...state,
        data_row: action.payload,
      };
    case SET_NAME:
      return {
        ...state,
        name: action.payload,
      };
    case SET_MOBILE:
      return {
        ...state,
        mobile: action.payload,
      };
    case SET_EMAIL:
      return {
        ...state,
        email: action.payload,
      };
    case ADD_ITEMS_TO_ARRAY:
      return {
        ...state,
        data_row: action.payload
      }
      case DELETE_ITEMS_TO_ARRAY:
        return {
          ...state,
          data_row: action.payload
        }
        case EDIT_ITEMS_TO_ARRAY:
          return {
            ...state,
            data_row: action.payload
          }        

    default:
      return state;
  }
}
