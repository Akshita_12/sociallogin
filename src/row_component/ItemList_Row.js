import React, { Component } from "react";
import { FlatList, Image, Text, TouchableOpacity, View } from "react-native";

class ItemList_Row extends Component {


    render() {
        return (
            <View style={{ margin: 10, backgroundColor: 'white', borderRadius: 10, elevation: 10, padding: 10 }} >

                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'flex-end' }}>
                    <Text>
                        {this.props.data_row.name}
                    </Text>
                    <TouchableOpacity onPress={() => { this.props.onDelete(this.props.data_row.email) }}>
                        <Image
                            style={{ height: 15, width: 15 }}
                            source={require('../assets/delete.png')}
                        />
                    </TouchableOpacity>
                </View>

                <Text>
                    {this.props.data_row.mobile}
                </Text>


                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'flex-end' }}>
                    <Text>
                        {this.props.data_row.email}
                    </Text>
                    <TouchableOpacity
                     onPress={() => { this.props.onEdit( this.props.data_row.email,this.props.data_row.name,this.props.data_row.mobile) }}
                    >
                        <Text>Edit</Text>
                    </TouchableOpacity>
                </View>

            </View>
        )
    }
}

export default ItemList_Row;