import React from 'react';
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';

const Options = (props)=>{
    return(
        <TouchableOpacity
        style ={[styles.mainViewStyle,styles.myshadow]}
        onPress={props.onPress}
        >
     
            <Image style={styles.imgStyle}
             source={props.image}
           />
            <Text style={styles.textStyle}>
                {props.text}
            </Text>
        </TouchableOpacity>


    )

}
const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    mainViewStyle: {
      flexDirection: 'row',
      height: 60,
      backgroundColor: 'white',
      marginHorizontal:5,
      marginTop: 11,
      borderRadius: 11,
    },
  
    myshadow:{
      shadowColor:"#0008",
      shadowOffset:{
        height:1,
        width:0
        
      },
      shadowOpacity:0.5,
      elevation: 11,
    },
    imgStyle: {
      height: 35,
      width: 35,
      borderRadius: 100,
      marginTop: 15,
      marginLeft: 15,
    },
    textStyle: {
      marginLeft: 30,
      fontSize: 16,
      fontWeight: 'normal',
      color:'#3D3D3D',
      alignSelf: 'center',
      fontFamily:"SourceSansPro"
    },
    //   rowStyle: {
    //     alignItems: 'center',
    //     flex: 1,
    //     borderRightWidth: 0.5,
    //     paddingVertical: 10,
    //   },
  });
export default Options