import React from 'react';
import { Text, TouchableOpacity } from 'react-native';

const MyButton = (props) => {
    return (
        <TouchableOpacity
            onPress={props.onPress}
            style={{
                height: 60,
                width: '70%',
                elevation: 5,
                borderRadius: 5,
                backgroundColor: 'green',
                justifyContent:'center',
                alignItems:'center',
                alignSelf:'center'
            }}
        >
            <Text
            style={{color:'white'}}
            > 
                {props.text}
            </Text>
        </TouchableOpacity>
    )
}

export default MyButton;