import React from 'react';
import { TextInput } from 'react-native';
const MyTextInput = (props) => {
    return (
        <TextInput
        placeholder={props.placeholder}
        placeholderTextColor={props.placeholderTextColor}
        keyboardType={props.keyboardType}
        onChangeText={props.onChangeText}
        maxLength={props.maxLength}
        value={props.value}
        editable={props.editable}
        style={{
            height:70,
            margin:10,
            padding:10,
            backgroundColor:"white",
            elevation:2,
            borderBottomWidth:1,
            borderColor:'grey'
        }}
        />
    )
}
export default MyTextInput;