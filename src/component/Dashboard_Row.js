import React from 'react';
import { Image, StyleSheet, Text, TouchableOpacity } from 'react-native';

const Dashboard_Row = (props) => {
    return (
        <TouchableOpacity 
        onPress={()=>props.onClick(props.data_row.screen_name)}
        style={styles.touchStyle}>
            <Image
                source={props.data_row.icon}
                style={styles.imgStyle}
            />
            <Text style={styles.txtStyle}>
                {props.data_row.screen_name}
            </Text>

        </TouchableOpacity>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fafaff',
    },
    touchStyle: {
        flexDirection: 'row',
        height: 70,
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center',
        elevation:2,
        marginTop:10,
        marginHorizontal:5
    },
    imgStyle: {
        height: 45,
        width: 45,
        margin: 10
    },
    txtStyle: {
        fontSize: 16,
        fontFamily: 'serif'
    },

    myshadow: {
        shadowColor: '#0008',
        shadowOffset: {
            height: 1,
            width: 0,
        },
        shadowOpacity: 0.5,
        elevation: 11,
    },

});
export default Dashboard_Row;