import React, {Component} from 'react';
import {View, Text, Image} from 'react-native';

export default class Splash extends Component {
  componentDidMount() {
    setTimeout(() => {
      this.props.navigation.navigate('Dashboard');
    }, 2000);
  }

  
  render() {
    return (
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          flex: 1,
          backgroundColor: 'azure',
        }}>
        <Image
          source={require('../assets/react_icon.png')}
          style={{width: 200, height: 200}}
        />
      </View>
    );
  }
}
