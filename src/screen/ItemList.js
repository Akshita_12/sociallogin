import React, { Component } from "react";
import { FlatList, Text, View } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { deleteItemsToArray, setEmail, setMobile, setName } from "../redux_store/actions/indexActions";
import ItemList_Row from "../row_component/ItemList_Row";

class ItemList extends Component {

onDelete=(email)=>{
    this.props.deleteItemsToArray(email)
}

_onAdd=()=>{
    this.props.navigation.navigate('AddItems')
}

onEdit = (email,name,mobile)=>{
    this.props.setName(name)
    this.props.setMobile(mobile)
    this.props.setEmail(email)
    this.props.navigation.navigate('EditItems')
}

_renderItem=(dataItem)=>{
    return(
        <ItemList_Row
         data_row={dataItem.item}
         onDelete={this.onDelete}
         onEdit={this.onEdit}
        />
    )}


    render() {
        return (
            <View>
                <View style={{height:50, alignItems:"flex-end",justifyContent:"center", backgroundColor:'green'}} >
                    <TouchableOpacity 
                    onPress={()=>{this._onAdd()}}
                    style={{padding:10}} >
                        <Text style={{color:"white",fontSize:20}} >add</Text>
                    </TouchableOpacity>
                </View>
                <FlatList
                    data={this.props.data_row.reverse()}
                    renderItem={this._renderItem}
                    keyExtractor={(item,index)=>'item'+index}
                />
            </View>
        )
    }
}
const mapStateToProps = (state) => {
    const s = state.indexReducer
    return {
        data_row:s.data_row,
    }
}
const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        setName: (name) => setName(name),
        setMobile: (mobile) => setMobile(mobile),
        setEmail: (email) => setEmail(email),
        deleteItemsToArray:(email) =>deleteItemsToArray(email)
        
    },
        dispatch,
    )
}
export default connect(mapStateToProps, mapDispatchToProps)(ItemList);