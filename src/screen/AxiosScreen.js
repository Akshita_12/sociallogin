import React from 'react'
import { Component } from "react";
import { Button, Text, TextInput, View } from 'react-native';
import axios from 'axios'
import { event } from 'react-native-reanimated';

class AxiosScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            // data: []
            name: ''
        }
    }
    componentDidMount() {
        // this.getApiData()
        // this.postApiData()
        // this.getApi()
    }

    // async getApiData() {
    //     let resp = await axios.get("https://reactnative.dev/movies.json")
    //     console.log(resp.data.movies);
    //     this.setState({ data: resp.data.movies })
    // }


    // async postApiData() {
    //     let resp = await axios.post("https://reactnative.dev/movies.json")
    //     console.log(resp.data.movies);
    //     this.setState({ data: resp.data.movies })
    // }

    // getApi = () => {
    //     axios.get("https://jsonplaceholder.typicode.com/users")
    //         .then(resp => {
    //             console.log(resp);
    //             this.setState({ data: resp.data })
    //         })
    // }

    handleSubmit = () => {
        let name = this.state.name
        axios.post("https://jsonplaceholder.typicode.com/users", { name })
            .then(resp => {
                console.log(resp);
                console.log(resp.data);
                alert(resp.data.name)
                // this.setState({ data: resp.data })
            })
    }

    handeleChanges = event => {
        this.setState({ name: event })
    }

    render() {
        return (
            <View>
                {/* {
                    this.state.data.length > 0 ?
                        <View>
                            {
                                this.state.data.map((item) =>
                                    <View style={{ height: 50, margin: 10, backgroundColor: "white", elevation: 5, justifyContent: 'center' }} >
                                        <Text>{item.name} </Text>
                                        <Text>{item.email} </Text>
                                    </View>
                                )
                            }
                        </View> : <Text>Data is loading...</Text>
                } */}

                <TextInput
                    placeholder="type name..."
                    onChangeText={(name) => this.handeleChanges(name)}
                />

                <Button 
                title="Save"
                onPress={()=>this.handleSubmit()}
                />

            </View>
        )
    }
}
export default AxiosScreen;