import React from 'react'
import { Component } from "react";
import { Button, Text, TextInput, View } from 'react-native';
import { LoginButton, AccessToken } from 'react-native-fbsdk';
import { LoginManager } from "react-native-fbsdk";

class FacebookSignUp extends Component {
    constructor(props) {
        super(props)
        this.state = {
            // data: []
            name: ''
        }
    }

    onLogin = ()=>{
      LoginManager.logInWithPermissions(["public_profile"]).then(
        function(result) {
          if (result.isCancelled) {
            console.log("Login cancelled");
          } else {
            console.log(
              "Login success with permissions: " +
                result.grantedPermissions.toString()
            );
          }
        },
        function(error) {
          console.log("Login fail with error: " + error);
        }
      );
    }
        

 
   

    render() {
        return (
            <View style={{justifyContent:'center',alignItems:'center',flex:1}}>
            
        <LoginButton
          onLoginFinished={
            (error, result) => {
              if (error) {
                console.log("login has error: " + result.error);
              } else if (result.isCancelled) {
                console.log("login is cancelled.");
              } else {
                AccessToken.getCurrentAccessToken().then(
                  (data) => {
                    console.log(data.accessToken.toString())
                  }
                )
              }
            }
          }
          onLogoutFinished={() => console.log("logout.")}/>
      </View>


        )
    }
}
export default FacebookSignUp;