import React, { Component } from "react";
import { FlatList, Text, View } from "react-native";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Dashboard_Row from "../component/Dashboard_Row";

class Dashboard extends Component {

    _onN = (screen)=>{
        if (screen == 'Flatlist') {
            this.props.navigation.navigate('ItemList')
        }else if (screen=='Axios') {
            this.props.navigation.navigate('AxiosScreen')
        }
        else if (screen=='GoogleSignUp') {
            this.props.navigation.navigate('GoogleSignUp')
        }
        else if (screen=='GoogleSignUpCommunity') {
            this.props.navigation.navigate('GoogleSignUpCommunity')
        }
        else if (screen=='GoogleSignUpCommunity') {
            this.props.navigation.navigate('GoogleSignUpCommunity')
        }
        else if (screen=='GoogleSignUpCommunity') {
            this.props.navigation.navigate('InstagramSignUp')
        }
        else if (screen=='FacebookSignUp') {
            this.props.navigation.navigate('FacebookSignUp')
        }
    }
    
    _renderItem = (item) => {
        return (
            <Dashboard_Row
                onClick={this._onN}
                data_row={item.item}
                // x={23}
            />
        )
    }

    render() {
        return (
            <View>
                <FlatList
                    data={this.props.dashboard_data}
                    renderItem={this._renderItem}
                />
            </View>
        )
    }
}
const mapStateToProps = (state) => {
    const s = state.indexReducer;
    return {
        dashboard_data: s.dashboard_data
    }
}
const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(
      {
       
      },
      dispatch,
    );
  };
export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);