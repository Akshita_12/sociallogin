import React,{ Component } from "react";
import { Image, Text, View } from "react-native";
import Options from "../component/Options";

class ReduxOperations extends Component{
    render(){
        return(
            <View style ={{flex: 1,backgroundColor:'#fafaff'}}>
                <Options
                 onPress={()=> this.props.navigation.navigate('ItemList')}
                 image={require('../assets/react_icon.png')}
                 text="Add"
                />
               <Options
                 onPress={()=>this.props.navigation.navigate('')}
                 image={require('../assets/react_icon.png')}
                 text="Delete"
                />
                <Options
                 onPress={()=> this.props.navigation.navigate('EditItems')}
                 image={require('../assets/react_icon.png')}
                 text="Edit"
                />
              
                
            </View>
        )
    }
}
export default ReduxOperations;