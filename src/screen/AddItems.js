import React, { Component } from "react";
import { Text, TextInput, View } from "react-native";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import MyButton from "../component/MyButton";
import MyTextInput from "../component/MyTextInput";
import { addItemsToArray, setEmail, setMobile, setName } from "../redux_store/actions/indexActions";

class AddItems extends Component {


    render() {
        return (
            <View>
                <MyTextInput
                    placeholder=" Enter name"
                    placeholderTextColor="grey"
                    keyboardType="email-address"
                    maxLength={20}
                    value={this.props.name}
                    onChangeText={(txt) => {
                        this.props.setName(txt)
                    }}
                />
                <MyTextInput
                    placeholder=" Enter mobile"
                    placeholderTextColor="grey"
                    keyboardType="numeric"
                    maxLength={10}
                    value={this.props.mobile}
                    onChangeText={(txt) => {
                        this.props.setMobile(txt)
                    }}
                />
                <MyTextInput
                    placeholder=" Enter email"
                    placeholderTextColor="grey"
                    keyboardType="email-address"
                    value={this.props.email}
                    maxLength={10}
                    onChangeText={(txt) => {
                        this.props.setEmail(txt)
                    }}
                />

                <MyButton
                    onPress={() => {
                        this.props.addItemsToArray(this.props.name,this.props.mobile,this.props.email)
                        this.props.navigation.goBack() }
                    }

                    text="Save"
                />
            </View>
        )
    }
}

const mapStateToProps = (state) => {
    const s = state.indexReducer;
    return {
        name: s.name,
        mobile: s.mobile,
        email: s.email
    }
}
const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        setName: (name) => setName(name),
        setMobile: (mobile) => setMobile(mobile),
        setEmail: (email) => setEmail(email),
        addItemsToArray:(name,mobile,email) => addItemsToArray(name,mobile,email)
    },
        dispatch,
    )
}
export default connect(mapStateToProps, mapDispatchToProps)(AddItems);