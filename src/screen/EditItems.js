import React, { Component } from 'react';
import { View, Text, Image } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import MyButton from '../component/MyButton';
import MyTextInput from '../component/MyTextInput';
import { deleteItemsToArray, EditItemsToArray, setEmail, setMobile, setName } from '../redux_store/actions/indexActions';

class EditItems extends Component {
    render() {
        return (
            <View
                style={{
                    // alignItems: 'center',
                    justifyContent: 'center',
                    flex: 1,
                    backgroundColor: 'azure',
                }}>
                <MyTextInput
                    placeholder=" Enter name"
                    placeholderTextColor="grey"
                    keyboardType="email-address"
                    maxLength={20}
                    value={this.props.name}
                    onChangeText={(txt) => {
                        this.props.setName(txt)
                    }}
                />
                <MyTextInput
                    placeholder=" Enter mobile"
                    placeholderTextColor="grey"
                    keyboardType="numeric"
                    maxLength={10}
                    value={this.props.mobile}
                    onChangeText={(txt) => {
                        this.props.setMobile(txt)
                    }}
                />
                <MyTextInput
                    placeholder=" Enter email"
                    placeholderTextColor="grey"
                    keyboardType="email-address"
                    value={this.props.email}
                    maxLength={50}
                    editable={false}
                    onChangeText={(txt) => {
                        this.props.setEmail(txt)
                    }}
                />

                <MyButton
                    onPress={() => {
                        this.props.EditItemsToArray(this.props.name, this.props.email, this.props.mobile)
                        this.props.navigation.goBack()
                    }
                    }

                    text="Save"
                />
            </View>
        );
    }
}


const mapStateToProps = (state) => {
    const s = state.indexReducer
    return {
        data_row:s.data_row,
        email:s.email,
        name:s.name,
        mobile:s.mobile,
    }
}
const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        setName: (name) => setName(name),
        setMobile: (mobile) => setMobile(mobile),
        setEmail: (email) => setEmail(email),
        EditItemsToArray:(name,email,mobile)=>EditItemsToArray(name,email,mobile)
        
    },
        dispatch,
    )
}
export default connect(mapStateToProps, mapDispatchToProps)(EditItems);