/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from "react";
import { View, StyleSheet, StatusBar, Platform } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import store from "./src/redux_store/store";
import { Provider } from "react-redux";
import Splash from "./src/screen/Splash";
import Dashboard from "./src/screen/Dashboard";
import ReduxOperations from "./src/screen/ReduxOperations";
import ItemList from "./src/screen/ItemList";
import AddItems from "./src/screen/AddItems";
import EditItems from "./src/screen/EditItems";
import AxiosScreen from "./src/screen/AxiosScreen";
// import GoogleSignUp from "./src/screen/GoogleSignUp";
import GoogleSignUpCommunity from "./src/screen/GoogleSignUpCommunity";
import FacebookSignUp from "./src/screen/FacebookSignUp";



const Stack = createStackNavigator();

function App() {
  return (
    <Provider store={store}>
      <View style={styles.container}>
        <StatusBar backgroundColor="#9f00ff" />

        {/* <StatusBar backgroundColor={"#ffa"}/> */}

        {Platform.OS === "ios" && (
          <StatusBar backgroundColor="red" barStyle="light-content" />
        )}
        <NavigationContainer>
          <Stack.Navigator screenOptions={{ headerShown: false }}>
            <Stack.Screen name="Splash" component={Splash} />
            <Stack.Screen name="Dashboard" component={Dashboard} />
            <Stack.Screen name="ReduxOperations" component={ReduxOperations} />
            <Stack.Screen name="AddItems" component={AddItems} />
            <Stack.Screen name="ItemList" component={ItemList} />
            <Stack.Screen name="EditItems" component={EditItems} />
            <Stack.Screen name='AxiosScreen' component={AxiosScreen} />
            {/* <Stack.Screen name='GoogleSignUp' component={GoogleSignUp} /> */}
            <Stack.Screen name='GoogleSignUpCommunity' component={GoogleSignUpCommunity} />
            <Stack.Screen name='FacebookSignUp' component={FacebookSignUp} />



          </Stack.Navigator>
        </NavigationContainer>
      </View>
    </Provider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "azure",
  },
});

export default App;